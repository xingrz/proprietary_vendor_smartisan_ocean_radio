DEVICE := ocean

MODEM_IMG := firmware-update/modem.img
ABL_IMG := firmware-update/abl.img
XBL_IMG := firmware-update/xbl.img
XBL_CONFIG_IMG := firmware-update/xbl_config.img

TIMESTAMP := $(shell strings $(MODEM_IMG) | grep -m 1 '"Time_Stamp"' | sed -n 's|.*"Time_Stamp": "\([^"]*\)"|\1|p')
VERSION := $(shell echo $(TIMESTAMP) | sed 's|[ :-]*||g')

HASH_ABL := $(shell openssl dgst -r -sha1 $(ABL_IMG) | cut -d ' ' -f 1)
HASH_XBL := $(shell openssl dgst -r -sha1 $(XBL_IMG) | cut -d ' ' -f 1)
HASH_XBL_CONFIG := $(shell openssl dgst -r -sha1 $(XBL_CONFIG_IMG) | cut -d ' ' -f 1)

TARGET := RADIO-$(DEVICE)-$(VERSION).zip

# Build
# ==========

.PHONY: build
build: assert inspect $(TARGET)
	@echo Size: $(shell stat -f %z $(TARGET))

$(TARGET): META-INF firmware-update
	zip -r9 $@ $^

# Clean
# ==========

.PHONY: clean
clean:
	rm -f *.zip

# Assert
# ==========
.PHONY: assert
assert: $(ABL_IMG) $(XBL_IMG) $(XBL_CONFIG_IMG)
ifneq ($(HASH_ABL), 2fe701dd46477943975f44fbc69c48c08ecca930)
	$(error SHA-1 of abl.img mismatch)
endif
ifneq ($(HASH_XBL), c0a9a067f07b4236d41c32e8ce6e669883dd55af)
	$(error SHA-1 of xbl.img mismatch)
endif
ifneq ($(HASH_XBL_CONFIG), 9dd8201213dcfb79654ad71700a1ff1f7d3446ce)
	$(error SHA-1 of xbl_config.img mismatch)
endif
	@echo Everything is ok.

# Inspect
# ==========

.PHONY: inspect
inspect: $(MODEM_IMG)
	@echo Target: $(TARGET)
	@echo Timestamp: $(TIMESTAMP)
